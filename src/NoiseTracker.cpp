#include "NoiseTracker.hpp"

NoiseTracker::NoiseTracker(int numberOfNoiseValueToSend) {
  this->numberOfNoiseValueToSend = numberOfNoiseValueToSend;
  this->lastValues = {};

  this->lastValue = 0;
  this->lowestValue = 0;
  this->highestValue = 0;
  this->averageValue = 0;

  pinMode(MIC_PIN, INPUT);
}

void NoiseTracker::Read() {
  double noiseAnalogValue = analogRead(MIC_PIN);

  int noiseValue;
  if (noiseAnalogValue >= 0 && noiseAnalogValue < 64) {
      noiseValue = Arduino_h::map(noiseAnalogValue, 0, 64, 0, 30);
  } else if (noiseAnalogValue >= 64 && noiseAnalogValue < 256) {
      noiseValue = Arduino_h::map(noiseAnalogValue, 65, 256, 30, 65);
  } else if (noiseAnalogValue >= 256 && noiseAnalogValue <= 1024) {
      noiseValue = Arduino_h::map(noiseAnalogValue, 256, 1024, 65, 100);
  }

  // Last Value
  this->lastValue = noiseValue;

  // Highest Value
  this->highestValue =
      (noiseValue > this->highestValue) ? noiseValue : this->highestValue;

  // Lowest Value
  if (this->lowestValue == 0) {
    this->lowestValue = lastValue;
  } else {
    this->lowestValue =
        (noiseValue < this->lowestValue) ? noiseValue : this->lowestValue;
  }

  // Last Values
  if (noiseValue != 0) {
    this->lastValues.push_back(noiseValue);
  }
  if (static_cast<int>(this->lastValues.size()) ==
      this->numberOfNoiseValueToSend + 1)
    this->lastValues.pop_front();

  // Average Value
  double tmpValue = 0;
  for (auto value : this->lastValues) {
    tmpValue += value;
  }
  this->averageValue = tmpValue / this->lastValues.size();
}

void NoiseTracker::Display() {
  String lastValuesStr;
  for (auto value : this->lastValues) {
    lastValuesStr += String(value);
    if (value != static_cast<int>(this->numberOfNoiseValueToSend) - 2) {
      lastValuesStr += CSV_ARRAY_SEPARATOR;
    }
  }
}

void NoiseTracker::SendLoRa(int channel = 1) {
  String lastValuesStr;
  for (auto value : this->lastValues) {
    lastValuesStr += String(value);
    if (value != static_cast<int>(this->numberOfNoiseValueToSend)) {
      lastValuesStr += CSV_ARRAY_SEPARATOR;
    }
  }

  // Exemple (with ';' as CSV_SEPARATOR) :
  // <lastValue>;<avgValue>;<highestValue>;<lowestValue>;<lastValues>
  String messageData =
      String(this->lastValue) + CSV_SEPARATOR + String(this->averageValue) +
      CSV_SEPARATOR + String(this->highestValue) + CSV_SEPARATOR +
      String(this->lowestValue) + CSV_SEPARATOR + "[" + lastValuesStr + "]";

  Serial.printf("AT+SEND=%d,%d,%s\r\n", channel, messageData.length(),
                messageData.c_str());
}

int NoiseTracker::GetLastValue() { return this->lastValue; }
int NoiseTracker::GetLowestValue() { return this->lowestValue; }
int NoiseTracker::GetHighestValue() { return this->highestValue; }
int NoiseTracker::GetAverageValue() { return this->averageValue; }
