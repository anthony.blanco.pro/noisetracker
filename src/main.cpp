#include <Arduino.h>
#include <math.h>

#include "NoiseTracker.hpp"
#include "Strip.hpp"

#define NUMBER_OF_VALUES_TO_SEND 3
#define LORA_CHANNEL 1

#define PIXELS_PIN 2
#define PIXELS_NUMBER 16
#define PIXELS_BRIGHTNESS 150

Adafruit_NeoPixel strip =
    Adafruit_NeoPixel(PIXELS_NUMBER, PIXELS_PIN, NEO_RGB + NEO_KHZ800);
NoiseTracker* nt;

#define PIXELS_COLOR_POWER strip.Color(0, 0, 255)
#define PIXELS_COLOR_BASE strip.Color(230, 0, 0)

#define LOW_PERCENTAGE 20
#define PIXELS_COLOR_LOW strip.Color(255, 255, 0)

#define MEDIUM_PERCENTAGE 45
#define PIXELS_COLOR_MEDIUM strip.Color(100, 200, 0)

#define HIGH_PERCENTAGE 80
#define PIXELS_COLOR_HIGH strip.Color(0, 255, 0)

void writeNoise(int pixelsToLight) {
  strip.clear();

  int vl = (int)(LOW_PERCENTAGE / 100.0 * PIXELS_NUMBER);
  int vm = (int)(MEDIUM_PERCENTAGE / 100.0 * PIXELS_NUMBER);
  int vh = (int)(HIGH_PERCENTAGE / 100.0 * PIXELS_NUMBER);

  strip.setPixelColor(0, PIXELS_COLOR_POWER);

  for (int pixelIdx = 1; pixelIdx < pixelsToLight; pixelIdx++) {
    if (pixelIdx > 0 && pixelIdx <= vl) {
      strip.setPixelColor(pixelIdx, PIXELS_COLOR_BASE);
    } else if (pixelIdx > vl && pixelIdx <= vm) {
      strip.setPixelColor(pixelIdx, PIXELS_COLOR_LOW);
    } else if (pixelIdx > vm && pixelIdx <= vh) {
      strip.setPixelColor(pixelIdx, PIXELS_COLOR_MEDIUM);
    } else if (pixelIdx > vh) {
      strip.setPixelColor(pixelIdx, PIXELS_COLOR_HIGH);
    }
  }
  
  strip.show();
}

std::vector<int> lastValues;

void setAllColors(uint32_t color) {
  for (int pixelIdx = 0; pixelIdx < PIXELS_NUMBER; pixelIdx++) {
    strip.setPixelColor(pixelIdx, color);
  }
}

void setup() {
  Serial.begin(115200);

  strip.setBrightness(PIXELS_BRIGHTNESS);
  strip.begin();

  nt = new NoiseTracker(NUMBER_OF_VALUES_TO_SEND);
}

void loop() {
  nt->Read();
  if (millis() % (1000 * 60) == 0) nt->SendLoRa(LORA_CHANNEL);

  currAvgValue = nt->GetAverageValue();
  if (currAvgValue > 5)
    writeNoise(Arduino_h::map(currAvgValue * 1.3, MIC_DB_MIN, MIC_DB_MAX, 0,
                              PIXELS_NUMBER));
}
