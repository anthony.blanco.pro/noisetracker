#include "Strip.hpp"

Strip::Strip(int pin, int pixelNumber, int pixelBrightness) {
  this->strip = Adafruit_NeoPixel(pixelNumber, pin, NEO_RGB + NEO_KHZ800);
  this->strip.setBrightness(pixelBrightness);
  this->strip.begin();
  this->pixelNumber = pixelNumber;
};

void Strip::Clear() { this->strip.clear(); }
void Strip::Draw() {
  for (int i = 0; i < this->pixelNumber; i++) {
    Serial.printf("%d - ", this->GetPixels()[i]);
  }
  this->strip.show();
};

uint8_t* Strip::GetPixels() { return this->strip.getPixels(); };

void Strip::SetPixels(std::map<int, uint32_t> pixels) {
  for (auto it = pixels.begin(); it != pixels.end(); ++it) {
    this->strip.setPixelColor(it->first, it->second);
  }
};

void Strip::SetPixel(int pixelIdx, uint32_t color) {
  this->strip.setPixelColor(pixelIdx, color);
};

uint32_t Strip::Color(uint16_t r, uint16_t g, uint16_t b) {
  return this->strip.Color(r, g, b);
}
