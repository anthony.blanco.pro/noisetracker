#include <Arduino.h>

#include <ctime>
#include <list>
#include <vector>

using namespace std;

#ifndef __NOISETRACKER_H__
#define __NOISETRACKER_H__

#define MIC_PIN A0

#define CSV_SEPARATOR ';'
#define CSV_ARRAY_SEPARATOR ','

class NoiseTracker {
 private:
  int numberOfNoiseValueToSend;

  std::list<int> lastValues;
  int lastValue;
  int highestValue;
  int lowestValue;
  int averageValue;

  bool isConfigured;

 public:
  NoiseTracker(int numberOfNoiseValueToTrack);

  void Read();
  void Display();

  void SendLoRa(int channel);

  int GetLastValue();
  int GetLowestValue();
  int GetHighestValue();
  int GetAverageValue();
};

#endif
