#include <map>

#include "Adafruit_NeoPixel.h"

#ifndef __STRIP_H__
#define __STRIP_H__

class Strip {
 private:
  Adafruit_NeoPixel strip;
  int pixelNumber;

 public:
  Strip(int pin, int pixelNumber, int pixelBrightness);

  void Draw();
  void Clear();
  uint32_t Color(uint16_t r, uint16_t g, uint16_t b);

  uint8_t* GetPixels();
  void SetPixels(std::map<int, uint32_t> pixels);
  void SetPixel(int pixelIdx, uint32_t color);
};

#endif
